#BitScope Python Server

An example pass-through [BitScope Server][1] (server\.py) and a command line client (client\.py) written in [Python 2.x][5]. It requires Python and the following modules:

    $ sudo apt-get install python-setuptools
    $ sudo easy_install pip
    $ sudo pip install pyserial

It has been tested to work with [Raspberry Pi][3] and a [Debian Stable][4] Linux system. It should run on anything (Windows, Mac etc) that supports these prerequisites.

To use it, start the server on your computer at a command prompt:

    $ python3 server.py

and run [BitScope DSO][6] choosing \[1] **Ethernet** and \[2] **localhost**:

![setup][2]

or  *change* **localhost** to IP address of the host on which server is running.

If no signal is connected to the CH-A input a flat yellow trace will appear on the display. Connect an AC signal to the CH-A input of about 1V or more (up to 5V) and you should see it appear on the display.

New to BitScope? See bitscope.com/start to get started.

  [1]: http://bitscope.com/software/server
  [2]: https://bytebucket.org/bitscope/python-server/raw/1444bff82fb11cd0cadd62ba4f1992b70a2b1a9e/setup.png
  [3]: http://raspberrypi.org/
  [4]: https://www.debian.org/releases/stable/
  [5]: https://docs.python.org/2/
  [6]: http://bitscope.com/software/dso/