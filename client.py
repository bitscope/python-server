import sys
import time

import socket
import threading

# Ask user for IP address
serverIp = raw_input("IP to connect to: ")
serverAddress = (serverIp, 16385)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.settimeout(0.1)

cmdId = 0

running = True

commandBuffer = ""
receiveBuffer = ""


def sendCommand():
    global cmdId
    global commandBuffer
    while running:
        command = raw_input()
        try:
            command = chr(cmdId) + command
            cmdId += 1
            if cmdId >= 256:
                cmdId = 0
            commandBuffer += command
        except:
            pass
    
def receiveReply():
    global receiveBuffer
    while running:
        try:
            data, server = sock.recvfrom(4096)
            receiveBuffer += data
        except:
            pass

def main():
    global commandBuffer, receiveBuffer
    
    sendThread = threading.Thread(target=sendCommand)
    receiveThread = threading.Thread(target=receiveReply)
    sendThread.start()
    receiveThread.start()
    
    while running:
        if commandBuffer:
            sent = sock.sendto(commandBuffer, serverAddress)
            commandBuffer = ""
        if receiveBuffer:
            print "REPLY: ", receiveBuffer[4:].replace("\r", "\n")
            receiveBuffer = ""
        
        time.sleep(0.1)

main()
