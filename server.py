import sys
import time

import socket
import serial

ser = serial.Serial("/dev/ttyUSB0", 115200, timeout=0.1)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Internet, UDP
sock.settimeout(0.0)
ip = "0.0.0.0"
port = 16385 # Listen (HEY)

sock.bind((ip, port))

verbose = False
superVerbose = False

address = None

cmd = "\x00"
dataId = 0

def receiveCommand():
    # Command
    global address
    global cmd
    try:
        dataIn, addr = sock.recvfrom(256)
        address = addr
        ser.write(dataIn[1:])
        cmd = dataIn[0]
        if verbose or superVerbose:
            print "RECV:", address, ":\n", dataIn, "\n"
            if superVerbose:
                print "LIST:", "\n", list(dataIn), "\n"
    except:
        pass
    
def sendReply():
    # Reply
    global dataId
    dataOut = ser.read(ser.inWaiting())
    if address and dataOut:
        dataOut = "\x00\x00" + cmd + chr(dataId) + dataOut
        dataOutBuffer = buffer(dataOut)
        dataId += 1
        if dataId >= 256:
            dataId = 0
        # print bytearray(dataOut).count()
        sock.sendto(dataOutBuffer, address)
        if verbose or superVerbose:
            print "SENT:", address, ":\n", dataOut.replace("\r", "\n"), "\n"
            if superVerbose:
                print "LIST:", "\n", list(dataOutBuffer), "\n"
    
def main():
    running = True
    while running:
        receiveCommand()
        time.sleep(0.001)
        sendReply()
        time.sleep(0.001)

main()
